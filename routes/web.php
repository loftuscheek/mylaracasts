<?php

// home
Route::get('/', 'PostController@getIndex')->name('home');
// single post
Route::get('posts/{post}', 'PostController@getShow')->name('post.single');

Route::get('posts/tag/{tag}', 'PostController@getPostsByTag');


Route::group(['middleware' => 'auth'], function (){
    //create a post
    Route::get('post/create', 'PostController@getCreate')->name('post.create');

    Route::post('post', 'PostController@postCreate')->name('post.create');
    //add comment
    Route::post('posts/{post}/comments', 'CommentController@postCreate')->name('comment.create');
    //user logout
    Route::get('user/logout', 'AuthController@getLogout')->name('auth.logout');

});

Route::group(['middleware' => 'guest'], function (){
    //login
    Route::get('user/login', 'AuthController@getLogin')->name('login');

    Route::post('user/login', 'AuthController@postLogin')->name('auth.login');
    //sign up
    Route::get('user/signup', 'AuthController@getSignup')->name('auth.signup');

    Route::post('user/signup', 'AuthController@postSignup')->name('auth.signup');

});