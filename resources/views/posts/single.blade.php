@extends('layouts.master')
@section('title', $post->title)
@section('content')
<div class="blog-post">
    <h2 class="blog-post-title">{{ $post->title }}</h2>

    <p class="blog-post-meta">{{ $post->created_at->toFormattedDateString() }} by <a href="#">{{ $post->user->name }}</a></p>

    @if(count($post->tags))
        <ul>
            @foreach ($post->tags as $tag)
                <li>{{ $tag->name }}</li>
            @endforeach
        </ul>
    @endif

    <p>{{ $post->body }}</p>

    <ul class="list-group">
        @foreach($post->comments as $comment)
            <li class="list-group-item">{{ $comment->body }}</li>
        @endforeach
    </ul>

    <hr>

    @include('partials.error')
    <div class="card">
        <div class="card-block">
            <form action="/posts/{{ $post->id }}/comments" method="post">
                {{ csrf_field() }}
                <div class="form-group">
                    <textarea name="body" class="form-control" placeholder="Your comment here..."></textarea>
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-primary" value="Send">
                </div>
            </form>
        </div>
    </div>
</div><!-- /.blog-post -->
@endsection