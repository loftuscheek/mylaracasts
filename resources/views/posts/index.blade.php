@extends('layouts.master')
@section('title', 'Home')
@section('content')
@if(session()->has('message'))
    <div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        {{ session()->get('message') }}
    </div>
@endif
@foreach($posts as $post)
<div class="blog-post">
    <h2 class="blog-post-title"><a href="/posts/{{ $post->id }}">{{ $post->title }}</a></h2>
    <p class="blog-post-meta">{{ $post->created_at->toFormattedDateString() }} by <a href="#">{{ ucfirst($post->user->name) }}</a></p>

    </p>
</div><!-- /.blog-post -->
@endforeach
@endsection