@extends('layouts.master')
@section('title', 'Create a post')
@section('content')
    <h1>Publish a post</h1>
    <hr>
    <form action="{{ route('post.create') }}" method="post">

        {{ csrf_field() }}

        <div class="form-group">
            <label for="title">Title:</label>
            <input type="text" name="title" class="form-control"> 
        </div>

        <div class="form-group">
            <label for="title">body:</label>
            <textarea name="body" class="form-control"></textarea>
        </div>

        <div class="form-group">
            <input type="submit" value="Publish" class="btn btn-primary"> 
        </div>

    </form>
    @include('partials.error')
@endsection