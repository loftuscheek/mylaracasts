@extends('layouts.master')
@section('title', 'Login')
@section('content')
    <h1>Login</h1>
    <hr>
    <form action="{{ route('auth.login') }}" method="post">

        {{ csrf_field() }}

        <div class="form-group">
            <label for="name">Name:</label>
            <input type="text" name="name" class="form-control"> 
        </div>

        <div class="form-group">
            <label for="password">Password:</label>
            <input type="password" name="password" class="form-control"> 
        </div>

        <div class="form-group">
            <input type="submit" value="Login" class="btn btn-primary"> 
        </div>

    </form>
    @include('partials.error')
@endsection