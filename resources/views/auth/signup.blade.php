@extends('layouts.master')
@section('title', 'Sign up')
@section('content')
    <h1>Sign up</h1>
    <hr>
    <form action="{{ route('auth.signup') }}" method="post">

        {{ csrf_field() }}

        <div class="form-group">
            <label for="name">Name:</label>
            <input type="text" name="name" class="form-control"> 
        </div>

        <div class="form-group">
            <label for="email">Email:</label>
            <input type="email" name="email" class="form-control"> 
        </div>

        <div class="form-group">
            <label for="password">Password:</label>
            <input type="password" name="password" class="form-control"> 
        </div>

        <div class="form-group">
            <input type="submit" value="Sign up" class="btn btn-primary"> 
        </div>

    </form>
    @include('partials.error')
@endsection