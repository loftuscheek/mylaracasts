<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new \App\User([
            'name' => 'admin',
            'email' => 'user@example.com',
            'password' => bcrypt('randompswd')
        ]);
        $user->save();
        $user = new \App\User([
            'name' => 'moderator',
            'email' => 'moderator@example.com',
            'password' => bcrypt('randompswd')
        ]);
        $user->save();
        $user = new \App\User([
            'name' => 'Pique',
            'email' => 'pique@example.com',
            'password' => bcrypt('randompswd')
        ]);
        $user->save();
    }
}
