<?php

use Illuminate\Database\Seeder;

class CommentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $comment = new \App\Comment([
            'body' => 'My first comment here.',
            'post_id' => 2,
            'user_id' => 3
        ]);
        $comment->save();
        $comment = new \App\Comment([
            'body' => 'Additional comment here.',
            'post_id' => 3,
            'user_id' => 1
        ]);
        $comment->save();
    }
}
