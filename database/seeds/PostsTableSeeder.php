<?php

use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $post = new \App\Post([
            'title' => 'Create new task',
            'body' => 'Create new task',
            'user_id' => 2
        ]);
        $post->save();
        $post = new \App\Post([
            'title' => 'Create test project',
            'body' => 'Create new project',
            'user_id' => 1
        ]);
        $post->save();
        $post = new \App\Post([
            'title' => 'Move to the repository',
            'body' => 'Repository',
            'user_id' => 2
        ]);
        $post->save();
    }
}
