<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class AuthController extends Controller
{
    /* public function __construct()
    {
        return $this->middleware('guest')->except(['getLogout', 'postLogin', 'postSignup']);
    } */
    
    // Sign up
    public function getSignup()
    {
        return view('auth.signup');
    }
    public function postSignup()
    {
        // validate the form
        $this->validate(request(), [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required'
        ]);
        // create and save the user
        $user = User::create([
            'name' => request('name'),
            'email' => request('email'),
            'password' => bcrypt(request('password'))
        ]);

        // sign them in
        auth()->login($user);

        //flash message
        session()->flash('message', 'Thanks for signing up!');

        // redirect
        return redirect()->home();
    }
    // Logout
    public function getLogout()
    {
        auth()->logout();

        return back();
    }
    // Login
    public function getLogin()
    {
        return view('auth.login');
    }
    public function postLogin()
    {
        // validate the form
        $this->validate(request(), [
            'name' => 'required',
            'password' => 'required'
        ]);

        // attempt to authenticate the user
        if(auth()->attempt(request(['name', 'password'])))
        {
            return redirect()->home();
        }

        // redirect
        return back()->withErrors([
            'message' => 'Please check your credentials and try again.'
        ]);
    }
}
