<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Post;
use App\Tag;

class PostController extends Controller
{
    /* public function __construct()
    {
        return $this->middleware('auth')->except(['getIndex', 'getShow']);
    } */
    
    public function getIndex()
    {
        $posts = Post::latest()
            ->filter(request(['month', 'year']))
            ->get();

        /* $posts = Post::latest();

        if($month = request('month')) {
            $posts->whereMonth('created_at', Carbon::parse($month)->month);
        }

        if($year = request('year')) {
            $posts->whereYear('created_at', $year);
        }

        $posts = $posts->get();*/

        return view('posts.index', compact('posts'));
    }

    public function getPostsByTag(Tag $tag)
    {
        $posts = $tag->posts;

        return view('posts.index', compact('posts'));
    }

    public function getCreate()
    {
        return view('posts.create');
    }

    public function postCreate()
    {
        
        $this->validate(request(), [
            'title' => 'required',
            'body' => 'required'
        ]);
        
        // old version of creating post by user
        /* Post::create([
            'title' => request('title'),
            'body' => request('body'),
            'user_id' => auth()->id()
        ]); */

        // good version
        auth()->user()->publish(
            new Post(request(['title', 'body']))
        );

        //flash message
        session()->flash('message', 'Thanks for signing up!');

        return redirect()->home();
    }

    public function getShow(Post $post)
    {
        return view('posts.single', compact('post'));
    }

}
